Algoritmo Ejercicio_13
	//Un a�o es bisiesto si es m�ltiplode 4, exceptuando los m�ltiplos de 100, que solo son bisiestos cuando son m�ltiplos adem�s de 400, 
	//por ejemplo el a�o 1900 no fue bisiesto, pero el a�o 2000 si lo fue. Hacer un diagrama de flujo que dado un a�o A determine si es bisiesto o no.
	escribir "Escriba el a�o que quiere saber si es bisiesto"
	leer a
	d=4
	c=100
	f=400
	r=a mod d
	t=a mod c
	u=a mod f
	si r=0 y t<>0 o u=0 entonces
		escribir "El a�o " a " es  bisiesto."
	sino 
		escribir "El a�o " a " no es bisiesto."
	FinSi
FinAlgoritmo
