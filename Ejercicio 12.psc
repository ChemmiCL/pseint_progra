Algoritmo Ejercicio_12
	//Dados los numero senteros positivos N y D, se dice qe d es un divisor de N si el resto de dividir N entre d es 0. se dice que un numero N es perfecto
	//si la suma de sus divisores (excluido el propio N) es N. por ejemplo 28 es perfecto pues sus divisores (exluido el 28) son 1,2,4,7 y 14 y su suma es 1+2+4+7+14=28,
	//realizar un diagrama de flujo que determina dado un numero N si es perfecto no no.
	//IR SUMANDO 1 EN 1 E IR VIENDO LOS RESTOS, PARA CUANDO R=0 ESCRIBIRLO, HASTA LLEGAR AL MISMO NUMERO N 
	Escribir "Escriba un n�mero para calcular su perfecci�n"
	leer n
	a=1
	p=0
	repetir
		d=n/a
		r=n mod a
		si r==0 entonces
			escribir a " es un divisor de " n
			p=p+a
		FinSi
		a=a+1
	Hasta Que a==n
	escribir "La suma de los divisores de " n " exceptuando " n " es: " p ", entonces:"
	si p==n Entonces
		escribir n " Es un n�mero perfecto."
	SiNo
		escribir n " NO es un n�mero perfecto."
	FinSi
	FinAlgoritmo
