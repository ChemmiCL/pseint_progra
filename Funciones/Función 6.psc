Funcion resultado <- tela ( metros )
	resultado<- metros/0.0254
Fin Funcion

Algoritmo Ejercicio_6
	escribir "Ingrese la cantidad de metros que quiere pedir"
	leer metros
	escribir "La cantidad de pulgadas que debe pedir es " tela(metros) " pulgadas."
	//una modista, para realizar sus prendas de vestir, encarga las telas al extranjero. Para cada pedido, tiene que proporcionar las medidas de la tela en pulgadas, 
	//pero ella generalmente la tiene en metros. Realice un algoritmo para ayudar a resolver el problema, determinando cuantas pulgadas debe pedir con base 
	//en los metros que requiere. Representelo mediante el diagrama de flujo y el pseudocodigo (1 pulgada=0,0254m)
FinAlgoritmo
