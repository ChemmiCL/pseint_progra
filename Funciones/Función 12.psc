Funcion res <- hipotenusa ( lado1, lado2 )
	res<-lado1^2+lado2^2
Fin Funcion

Algoritmo Ejercicio_12
//Se requiere determinar la hipotenusa de un tri�ngulo rect�ngulo. �C�mo ser�a el diagrama de flujo y el pseudoc�digo que representen el algoritmo para obtenerla? 
	//Recuerde que por Pit�goras se tiene que: C2 = A2 + B2.
	escribir "Escriba los ambos lados del tri�ngulo rect�ngulo"
	leer lado1, lado2
	Escribir "La hipotenusa del tri�ngulo rect�ngulo es " rc(hipotenusa(lado1, lado2))
FinAlgoritmo
